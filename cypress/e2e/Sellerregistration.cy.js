describe('Seller Registration', () => {
    it('Registers a new seller', () => {
      cy.visit('https://admin.meroshopping.com/register');
      
      // Fill out the registration form
      cy.get('#name').type('nora');
      cy.get(':nth-child(3) > .col-md-6 > #contact').type('9813384203');
      cy.get(':nth-child(4) > .col-md-6 > #contact').type('nuk');
      cy.get('#email').type('junar@gmail.com');
      cy.get('#password').type('password123');
      cy.get('#password-confirm').type('password123');
      
      // Submit the form
      cy.get('.btn').click();
      
      // Assert registration success
      cy.url().should('include', 'https://admin.meroshopping.com/verify-kyc'); 
      
    });


    it('should show error for using the same email to register', () => {
      cy.visit('https://admin.meroshopping.com/register');
      
      // Fill out the registration form
      cy.get('#name').type('aora');
      cy.get(':nth-child(3) > .col-md-6 > #contact').type('9811313200');
      cy.get(':nth-child(4) > .col-md-6 > #contact').type('ouk');
      cy.get('#email').type('nuk@gmail.com');
      cy.get('#password').type('password123');
      cy.get('#password-confirm').type('password123');
      
      // Submit the form
      cy.get('.btn').click();
      
      // Assert error
      //cy.get('.error-messages').should('contain', 'The email has been already taken') 
      
    });

      it('should show error for empty email', () => {
      cy.visit('https://admin.meroshopping.com/register');
      
      // Fill out the registration form
      cy.get('#name').type('aora');
      cy.get(':nth-child(3) > .col-md-6 > #contact').type('9811313200');
      cy.get(':nth-child(4) > .col-md-6 > #contact').type('ouk');
      cy.get('#email')
      cy.get('#password').type('password123');
      cy.get('#password-confirm').type('password123');
      
      // Submit the form
      cy.get('.btn').click();
      
      // Assert error
      //cy.get('.error-messages').should('contain', 'Please fill out this field') 
      
    });
     
    it('should show error for empty password', () => {
      cy.visit('https://admin.meroshopping.com/register');
      
      // Fill out the registration form
      cy.get('#name').type('pura');
      cy.get(':nth-child(3) > .col-md-6 > #contact').type('9811313200');
      cy.get(':nth-child(4) > .col-md-6 > #contact').type('ouk');
      cy.get('#email').type('jug@gmail.com')
      cy.get('#password')
      cy.get('#password-confirm').type('password123');
      
      // Submit the form
      cy.get('.btn').click();
      
      // Assert error
      //cy.get('.error-messages').should('contain', 'Please fill out this field') 
      
    });

    it('Registers with special characters in name', () => {
      cy.visit('https://admin.meroshopping.com/register');
      
      // Fill out the registration form
      cy.get('#name').type('iku@jui');
      cy.get(':nth-child(3) > .col-md-6 > #contact').type('9811313200');
      cy.get(':nth-child(4) > .col-md-6 > #contact').type('ouk');
      cy.get('#email').type('opoi@gmail.com');
      cy.get('#password').type('password123');
      cy.get('#password-confirm').type('password123');
      
      // Submit the form
      cy.get('.btn').click();
      
      cy.url().should('include', 'https://admin.meroshopping.com/verify-kyc'); 
       // Assuming successful registration redirects to kyc verification
      
    });
    

  });
  
// cypress/integration/registration.spec.js

describe('User Registration', () => {
    it('successfully registers with valid information', () => {
      cy.visit('https://www.meroshopping.com/register')
      
      // Fill in registration form
      cy.get('#input-firstname').type('John Karki')
      cy.get('#input-address').type('123 Main Street')
      cy.get('#input-email').type('john.doney@example.com')
      cy.get('#input-telephone').type('9843300585')
      cy.get('#input-password').type('password123')
      cy.get('#input-confirm').type('password123')
      
      
      // Submit the form
      cy.get('.pull-left > .btn').click()
      
      // Assert registration success
      cy.url().should('include','https://www.meroshopping.com/')
    })

    it('should show error for invalid email', () => {
      cy.visit('https://www.meroshopping.com/register')
      
      // Fill in registration form
      cy.get('#input-firstname').type('Joke Karki')
      cy.get('#input-address').type('123 Main Street')
      cy.get('#input-email').type('jokeexample.com')
      cy.get('#input-telephone').type('9842300585')
      cy.get('#input-password').type('password123')
      cy.get('#input-confirm').type('password123')
      
      
      // Submit the form
      cy.get('.pull-left > .btn').click()
      
      // Assert registration error
     
    })
    it('should show error for empty fullname', () => {
      cy.visit('https://www.meroshopping.com/register')
      
      // Fill in registration form
      cy.get('#input-firstname')
      cy.get('#input-address').type('123 Main Street')
      cy.get('#input-email').type('ok@example.com')
      cy.get('#input-telephone').type('9848700585')
      cy.get('#input-password').type('password123')
      cy.get('#input-confirm').type('password123')
      
      
      // Submit the form
      cy.get('.pull-left > .btn').click()
      
      // Assert registration error
    
    })

    
    it('successfully registers with take name', () => {
      cy.visit('https://www.meroshopping.com/register')
      
      // Fill in registration form
      cy.get('#input-firstname').type('John Karki')
      cy.get('#input-address').type('123 Main Street')
      cy.get('#input-email').type('oi@example.com')
      cy.get('#input-telephone').type('9843300085')
      cy.get('#input-password').type('password123')
      cy.get('#input-confirm').type('password123')
      
      
      // Submit the form
      cy.get('.pull-left > .btn').click()
      
      // Assert registration success
      cy.url().should('include','https://www.meroshopping.com/')
    })

    it('successfully registers with empty address information', () => {
      cy.visit('https://www.meroshopping.com/register')
      
      // Fill in registration form
      cy.get('#input-firstname').type('Jam Karki')
      cy.get('#input-address')
      cy.get('#input-email').type('jam@example.com')
      cy.get('#input-telephone').type('9843300581')
      cy.get('#input-password').type('password123')
      cy.get('#input-confirm').type('password123')
      
      
      // Submit the form
      cy.get('.pull-left > .btn').click()
      
      // Assert registration success
      cy.url().should('include','https://www.meroshopping.com/')
    })

  })
  
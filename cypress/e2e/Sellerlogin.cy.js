// cypress/integration/admin_login.spec.js

describe('Admin Login', () => {
    it('successfully logs in with valid credentials', () => {
      cy.visit('https://admin.meroshopping.com/login')
  
      // Fill in login form
      cy.get('input[name="email"]').type(' yuni82@gmail.com')
      cy.get('input[name="password"]').type('aaaaaaaa')

      // Submit the form
      cy.get('button[type="submit"]').click()
      // Assert login success
      cy.url().should('include', 'https://admin.meroshopping.com/verify-kyc')
    })



    it('should show error for invalid credentials', () => {
      cy.visit('https://admin.meroshopping.com/login')
      // Fill in login form
      cy.get('input[name="email"]').type(' yu2@gmail.com')
      cy.get('input[name="password"]').type('aaaaaaaa')

      // Submit the form
      cy.get('button[type="submit"]').click()
      // Assert login error
     // cy.get('strong').should('contain', 'These credentials donot match our records');
    })




    it('should show error for empty email', () => {
      cy.visit('https://admin.meroshopping.com/login')
      // Fill in login form
      cy.get('input[name="email"]')
      cy.get('input[name="password"]').type('aaaaaaaa')

      // Submit the form
      cy.get('button[type="submit"]').click()
      // Assert login error
     // cy.get('.error-messages').should('contain', 'Please fill out this field');
    })



    it('should show error for empty password', () => {
      cy.visit('https://admin.meroshopping.com/login')
      // Fill in login form
      cy.get('input[name="email"]').type(' yuni82@gmail.com')
      cy.get('input[name="password"]')

      // Submit the form
      cy.get('button[type="submit"]').click()
      // Assert login error
      //cy.get('.error-messages').should('contain', 'Please fill out this field');
    })
  })
  
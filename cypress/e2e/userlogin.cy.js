// cypress/integration/login.spec.j

describe('User Login', () => {
    it('successfully logs in with valid credentials', () => {
      cy.visit('https://www.meroshopping.com/login')
      // Fill in login form
      cy.get('#input-contact').type('yunika.baniya23@gmail.com')
      cy.get('#input-password').type('register1')
      cy.get('form > .btn').click()
      // Assert that the correct URL is visible
      cy.url().should('include', 'https://www.meroshopping.com/') // This line checks if the URL matches the homepage after login
   
    })
    

    it('should show error for invalid email', () => { 
      cy.visit('https://www.meroshopping.com/login')
      // Fill in login form
      cy.get('#input-contact').type('yun.baniya23@gmail.com')
      cy.get('#input-password').type('register1')
      cy.get('form > .btn').click()
      // Assert login error
     // cy.get('.error-messages').should('contain', 'email or password is invalid'); // This line checks if the URL matches the homepage after login
   
    })

    it('should show error for invalid password', () => {
      cy.visit('https://www.meroshopping.com/login')
      // Fill in login form
      cy.get('#input-contact').type('yun.baniya23@gmail.com')
      cy.get('#input-password').type('regi')
      cy.get('form > .btn').click()
      // Assert login error
      //cy.get('.error-messages').should('contain', 'email or password is invalid'); // This line checks if the URL matches the homepage after login
   
    })

    it('should show error logs for empty email', () => {
      cy.visit('https://www.meroshopping.com/login')
      // Fill in login form
      cy.get('#input-contact')
      cy.get('#input-password').type('register1')
      cy.get('form > .btn').click()
      // Assert login error
     // cy.get('.error-messages').should('contain', 'email or password is blank'); // This line checks if the URL matches the homepage after login
   
    })
    it('should show error for empty password', () => {
      cy.visit('https://www.meroshopping.com/login')
      // Fill in login form
      cy.get('#input-contact').type('yun.baniya23@gmail.com')
      cy.get('#input-password')
      cy.get('form > .btn').click()
      // Assert login error
      //cy.get('.error-messages').should('contain', 'email or password is blank'); // This line checks if the URL matches the homepage after login
   
    })


  })
